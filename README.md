CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

Simple module saves you a lot of time to put copyright block for your single/multi sites.
Date will auto change depends on your website's year.
Your site name copyrighted by will change if you change your site name.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/copyright_it

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/copyright_it


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


CONFIGURATION
-------------

 * Go to the block structure and place "Simple block with copyright information" block.


MAINTAINERS
-----------

Current maintainers:
 * Mutasem Elayyoub (sam-elayyoub) - https://www.drupal.org/u/sam-elayyoub
